package btvn1;

import java.util.Scanner;

public class sinhvien {
    private String mssv;
    private String name;
    private int age;
    private String gender;
    private String address;

    public sinhvien(String mssv, String name, int age, String gender, String address) {
        this.mssv = mssv;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.address = address;
    }

    public sinhvien(){

    }

    public String getMssv() {
        return mssv;
    }

    public void setMssv(String mssv) {
        this.mssv = mssv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void nhapTT(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập mssv ");
        mssv = sc.nextLine();
        System.out.println("Nhập tên : ");
        name = sc.nextLine();
        System.out.println("Nhập tuổi : ");
        age = sc.nextInt();
        System.out.println("Nhập giới tính : ");
        sc.nextLine();
        gender = sc.nextLine();
        System.out.println("Nhập địa chỉ : ");
        address = sc.nextLine();
    }

    public void hieenrThi(){
        System.out.println("MSSV : " + getMssv()+" Tên : " +getName()+" Tuổi: "+getAge()+" Giới tính : " + getGender()+" Địa chỉ : "+getAddress());
    }
    public void girl(){
        if(getGender() == "nữ"){
            hieenrThi();
        }
    }


}
