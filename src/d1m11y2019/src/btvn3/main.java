package btvn3;

import java.util.ArrayList;
import java.util.Scanner;

public class main {
    /*
    Bài 3: Tạo và thực hiện các chức năng sau của MENU:
1. Thêm vào 1 tuyến xe bus vào danh sách các tuyến xe bus. Biết rằng 1 tuyến xe bus gồm các thuộc tính sau: Mã tuyến xe, số xe, danh sách các con đường sẽ đi qua.
2. Hiển thị danh sách tên các tuyến xe.
3. Nhập vào tên 1 con đường và kiểm tra xem có bao nhiêu tuyến xe đi qua con đường đó. HIển thị danh sách các tuyến xe đó.
4. Nhập vào điểm đầu và điểm cuối là tên các con đường. Tìm lộ trình ngắn nhất để di chuyển. (có thể đi 1 tuyến xe hoặc nhiều tuyến xe).
5. Thoát
     */
    public static void main(String[] args) {
        bus b1 = new bus("01", "29B0000", "Yên Nghĩa");
        bus b2 = new bus("02", "29B0001", "Yên Nghĩa");
        bus b3 = new bus("03", "29B0021", "Yên Nghĩa");

        ArrayList<bus> bs = new ArrayList<>();
        bs.add(b1);
        bs.add(b2);
        bs.add(b3);
        Scanner sc = new Scanner(System.in);
        int chon = 1;
        int nhap;
        while (chon == 1) {
            System.out.println("1. Thêm vào 1 tuyến xe bus vào danh sách các tuyến xe bus. ");
            System.out.println("2. Hiển thị danh sách tên các tuyến xe.");
            System.out.println("3.. Nhập vào tên 1 con đường và kiểm tra xem có bao nhiêu tuyến xe đi qua con đường đó. HIển thị danh sách các tuyến xe đó.");
            System.out.println("4. Nhập vào điểm đầu và điểm cuối là tên các con đường. Tìm lộ trình ngắn nhất để di chuyển. (có thể đi 1 tuyến xe hoặc nhiều tuyến xe).");
            System.out.println("5. Thoát");
            System.out.println("nhập lựa chọn của bạn : ");
            nhap = sc.nextInt();
            switch (nhap) {
                case 1:
                    System.out.println("1. Thêm vào 1 tuyến xe bus vào danh sách các tuyến xe bus. ");
                    bus b4 = new bus("04", "29B0421", "Hà nội");
                    bs.add(b4);
                    break;
                case 2:
                    System.out.println("2. Hiển thị danh sách tên các tuyến xe.");
                    bs.forEach((obj) -> {
                        obj.hienThi();
                    });
                    break;
                case 3:
                    System.out.println("3.. Nhập vào tên 1 con đường và kiểm tra xem có bao nhiêu tuyến xe đi qua con đường đó. HIển thị danh sách các tuyến xe đó.");
                    System.out.println("Nhập tên con đường : ");
                    String ten = sc.nextLine();
                    bs.forEach((obj) -> {
                        if (obj.conduong.contains(ten) == true) {
                            obj.hienThi();
                        }
                    });
                    break;
                case 4:
                    System.out.println("4. Nhập vào điểm đầu và điểm cuối là tên các con đường. Tìm lộ trình ngắn nhất để di chuyển. (có thể đi 1 tuyến xe hoặc nhiều tuyến xe");
                case 5:
                    System.out.println("5.Thoát");
                    chon = 0;
            }
        }
    }
}
