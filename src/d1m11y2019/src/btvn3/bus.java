package btvn3;

public class bus {
    //Mã tuyến xe, số xe, danh sách các con đường sẽ đi qua.
    private String mabus;
    private String bienso;
   public String conduong;

    public bus(String mabus, String bienso, String conduong) {
        this.mabus = mabus;
        this.bienso = bienso;
        this.conduong = conduong;
    }

    public bus(String conduong) {
        this.conduong = conduong;
    }

    public bus(){

    }

    public void hienThi(){
        System.out.println("Mã tuyến xe : "+ mabus+" biển số : " + bienso+" conduong : " + conduong);
    }
}
