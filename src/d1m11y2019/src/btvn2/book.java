package btvn2;

import java.util.Scanner;

public class book {
    //Mã sách, mã tác giả, tên sách, số trang, ngày xuất bản, nhà xuất bản.

    private String ms;
    private String mtg;
    private String bname;
    private int dayxb;
    private int monthxb;
    private int yearxb;
    private String nhaxb;

    public book(String ms, String mtg, String bname, int dayxb, int monthxb, int yearxb, String nhaxb) {
        this.ms = ms;
        this.mtg = mtg;
        this.bname = bname;
        this.dayxb = dayxb;
        this.monthxb = monthxb;
        this.yearxb = yearxb;
        this.nhaxb = nhaxb;
    }
    public book(){

    }

    public String getMs() {
        return ms;
    }

    public void setMs(String ms) {
        this.ms = ms;
    }

    public String getMtg() {
        return mtg;
    }

    public void setMtg(String mtg) {
        this.mtg = mtg;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public int getDayxb() {
        return dayxb;
    }

    public void setDayxb(int dayxb) {
        this.dayxb = dayxb;
    }

    public int getMonthxb() {
        return monthxb;
    }

    public void setMonthxb(int monthxb) {
        this.monthxb = monthxb;
    }

    public int getYearxb() {
        return yearxb;
    }

    public void setYearxb(int yearxb) {
        this.yearxb = yearxb;
    }

    public String getNhaxb() {
        return nhaxb;
    }

    public void setNhaxb(String nhaxb) {
        this.nhaxb = nhaxb;
    }

    public void nhapTT(){
        //Mã sách, mã tác giả, tên sách, số trang, ngày xuất bản, nhà xuất bản.
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập mã sách : ");
        ms = sc.nextLine();
        System.out.println("Nhập mã tác giả : ");
        mtg = sc.nextLine();
        System.out.println("Nhập tên sách : ");
        bname = sc.nextLine();
        System.out.println("Nhập số ngày , tháng , năm : ");
        dayxb = sc.nextInt();
        sc.nextLine();
        monthxb=sc.nextInt();
        yearxb=sc.nextInt();
        System.out.println("Nhà sx: ");
        nhaxb = sc.nextLine();
        sc.nextLine();


    }

    public void hienThi(){
        System.out.println("Mã sách : "+getMs()+" Mã tác giả : "+getMtg()+" Tên sách: "+getBname()+" Ngày sb: "+dayxb + "/" + monthxb+"/"+ yearxb+" Nhà xb : "+nhaxb);
    }
}
