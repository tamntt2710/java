package btvn2;

import java.util.ArrayList;
import java.util.Scanner;

public class main {
    /*
    Bài 2: Tạo và thực hiện các chức năng sau của Menu:
1. Thêm 1 tác giả vào danh sách các tác giả, biết ràng 1 tác giả gồm các thuộc tính: Mã tác giả và tên tác giả.
2. Thêm 1 cuốn sách vào danh sách các cuốn sách biết rằng cuốn sách đó gồm các thuộc tính: Mã sách, mã tác giả, tên sách, số trang, ngày xuất bản, nhà xuất bản. Kiểm tra xem mã tác giả có tồn tại hay không? Nếu chưa có thì phải tạo tác giả rồi mới cho thêm vào danh sách.
3. Nhập vào tên 1 cuốn sách bất kì, kiểm tra xem cuốn sách đó có nằm trg danh sách hay không?
4. Nhập và tên 1 tác giả. Hiển thị ra tất cả các sách của tác giả đó viết.
5. Thoát
     */

    public static void main(String[] args) {
        author at1 = new author("Tâm","1");
        author at2 = new author("Hùng","2");
        author at3 = new author("Duy","3");

        ArrayList<author> at = new ArrayList<>();
        at.add(at1);
        at.add(at2);
        at.add(at3);

        ArrayList<book> bk = new ArrayList<>();
        book b2 = new book("1","1","Sống thực tế",10,27,2019,"Tuổi hồng");
        int nhap =  1;
        int chon;
        while (nhap == 1){
            System.out.println("1. Thêm 1 tác giả vào danh sách các tác giả, biết ràng 1 tác giả gồm các thuộc tính: Mã tác giả và tên tác giả.");
            System.out.println("2. Thêm 1 cuốn sách vào danh sách các cuốn sách biết rằng cuốn sách đó gồm các thuộc tính: Mã sách, mã tác giả, tên sách, số trang, ngày xuất bản, nhà xuất bản. Kiểm tra xem mã tác giả có tồn tại hay không? Nếu chưa có thì phải tạo tác giả rồi mới cho thêm vào danh sách.");
            System.out.println("3. Nhập vào tên 1 cuốn sách bất kì, kiểm tra xem cuốn sách đó có nằm trg danh sách hay không?");
            System.out.println("4. Nhập và tên 1 tác giả. Hiển thị ra tất cả các sách của tác giả đó viết");
            System.out.println("5. Thoát");
            System.out.println("Nhập lựa chọn của ban ");
            Scanner sc = new Scanner(System.in);
            chon = sc.nextInt();
            switch (chon){
                case 1:
                    System.out.println("1. Thêm 1 tác giả vào danh sách các tác giả, biết ràng 1 tác giả gồm các thuộc tính: Mã tác giả và tên tác giả.");
                    author at4 = new author();
                    at4.nhapTT();
                    at.add(at4);
                    break;
                case 2:
                    System.out.println("2. Thêm 1 cuốn sách vào danh sách các cuốn sách biết rằng cuốn sách đó gồm các thuộc tính: Mã sách, mã tác giả, tên sách, số trang, ngày xuất bản, nhà xuất bản. Kiểm tra xem mã tác giả có tồn tại hay không? Nếu chưa có thì phải tạo tác giả rồi mới cho thêm vào danh sách.");
                    book b1 = new book();
                    b1.nhapTT();
                    bk.add(b1);
                    author at5 = new author();
                        if(bk.contains(b1.getMtg()) == false){
                            at5.nhapTT();
                            at.add(at5);
                            b1.nhapTT();
                        }
                        else {
                            b1.nhapTT();
                        }
                break;
                case 3:
                    System.out.println("3. Nhập vào tên 1 cuốn sách bất kì, kiểm tra xem cuốn sách đó có nằm trg danh sách hay không?");
                    book b3 = new book();
                    b3.nhapTT();
                    bk.add(b3);
                    System.out.println("Kiểm tra : "+ bk.contains(b3));
                    break;
                case 4:
                    System.out.println("4. Nhập và tên 1 tác giả. Hiển thị ra tất cả các sách của tác giả đó viết");System.out.println("4. Nhập và tên 1 tác giả. Hiển thị ra tất cả các sách của tác giả đó viết");
                    System.out.println("Nhập tên tác giả bất kí : ");
                    String ten = sc.nextLine();
                    bk.forEach((obj)->{
                        if(obj.getMtg() == ten){
                            obj.hienThi();
                        }
                    });
                    break;
                case 5:
                    System.out.println("5. Thoát");
                    nhap = 0;


            }
        }

    }
}
