package act3;

import java.util.Scanner;

public class SinhVien {
    String mssv;
    String Name;
    String que;
    float da1;
    float da3;
    float ngly1;
    float tin;
    float laptrinh;

    public SinhVien(String mssv, String name, String que, float da1, float da3, float ngly1, float tin, float laptrinh) {
        this.mssv = mssv;
        Name = name;
        this.que = que;
        this.da1 = da1;
        this.da3 = da3;
        this.ngly1 = ngly1;
        this.tin = tin;
        this.laptrinh = laptrinh;
    }

    public String getMssv() {
        return mssv;
    }

    public void setMssv(String mssv) {
        this.mssv = mssv;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getQue() {
        return que;
    }

    public void setQue(String que) {
        this.que = que;
    }

    public float getDa1() {
        return da1;
    }

    public void setDa1(float da1) {
        this.da1 = da1;
    }

    public float getDa3() {
        return da3;
    }

    public void setDa3(float da3) {
        this.da3 = da3;
    }

    public float getNgly1() {
        return ngly1;
    }

    public void setNgly1(float ngly1) {
        this.ngly1 = ngly1;
    }

    public SinhVien(){

    }

    public float getTin() {
        return tin;
    }

    public void setTin(float tin) {
        this.tin = tin;
    }

    public float getLaptrinh() {
        return laptrinh;
    }

    public void setLaptrinh(float laptrinh) {
        this.laptrinh = laptrinh;
    }

    public void NhapTT(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhâp mssv : ");
        mssv = sc.nextLine();
        System.out.println("Nhập họ và tên : ");
        Name = sc.nextLine();
        System.out.println("Nhập quê quán : ");
        que= sc.nextLine();
        System.out.println("Nhập điểm A1 : ");
        da1 = sc.nextFloat();
        System.out.println("Nhập điểm A3 : ");
        da3 = sc.nextFloat();
        System.out.println("Nhập điểm nguyên lý : ");
        ngly1 = sc.nextFloat();
        System.out.println("Nhập điểm tin học đại cương :");
        tin = sc.nextFloat();
        System.out.println("Nhập điểm kĩ thuật lập trình : ");
        laptrinh = sc.nextFloat();
    }

    public void HienThiTT(){
        System.out.println("MSSV : " + this.mssv);
        System.out.println("Họ tên : " +this.Name);
        System.out.println("Quê quán : "+this.que);
        System.out.println("Điểm A1 : "+this.da1);
        System.out.println("Điểm A3 : " +this.da3);
        System.out.println("Điểm nguyên lý : "+ this.ngly1);
        System.out.println("Điểm tin học đại cương : " +this.tin);
        System.out.println("Điểm kĩ thuật lập trình : " +this.laptrinh);
    }
    public double Tinhdtb(){
        float diemTB = (da1+da3+ngly1)/3;
        return diemTB;
    }
    public float max(){
        float max = da1;
        if(max < da3){
            max = da3;
        }
        if(max < ngly1){
            max = ngly1;
        }
        if(max < tin){
            max = tin;
        }
        if(max < laptrinh){
            max = laptrinh;
        }
        return max;
    }
    public float min (){
        float min = da1;
        if(min > da3){
            min = da3;
        }
        if(min > ngly1){
            min = ngly1;
        }
        if(min > tin){
            min = tin;
        }
        if(min > laptrinh){
            min = laptrinh;
        }
        return min;
    }


}
