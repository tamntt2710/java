package act1;

import java.util.Scanner;

public class SinhVien {
    String mssv;
    String Name;
    String que;
    float da1;
    float da3;
    float ngly1;

    public SinhVien(String mssv, String name, String quequam, float da1, float da3, float ngly1) {
        this.mssv = mssv;
        this.Name = name;
        this.que= que;
        this.da1 = da1;
        this.da3 = da3;
        this.ngly1 = ngly1;
    }

    public String getMssv() {
        return mssv;
    }

    public void setMssv(String mssv) {
        this.mssv = mssv;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getQue() {
        return que;
    }

    public void setQue(String que) {
        this.que = que;
    }

    public float getDa1() {
        return da1;
    }

    public void setDa1(float da1) {
        this.da1 = da1;
    }

    public float getDa3() {
        return da3;
    }

    public void setDa3(float da3) {
        this.da3 = da3;
    }

    public float getNgly1() {
        return ngly1;
    }

    public void setNgly1(float ngly1) {
        this.ngly1 = ngly1;
    }

    public SinhVien(){

    }

    public void NhapTT(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhâp mssv : ");
        mssv = sc.nextLine();
        System.out.println("Nhập họ và tên : ");
        Name = sc.nextLine();
        System.out.println("Nhập quê quán : ");
        que= sc.nextLine();
        System.out.println("Nhập điểm A1 : ");
        da1 = sc.nextFloat();
        System.out.println("Nhập điểm A3 : ");
        da3 = sc.nextFloat();
        System.out.println("Nhập điểm nguyên lý : ");
        ngly1 = sc.nextFloat();
    }

    public void HienThiTT(){
        System.out.println("MSSV : " + this.mssv);
        System.out.println("Họ tên : " +this.Name);
        System.out.println("Quê quán : "+this.que);
        System.out.println("Điểm A1 : "+this.da1);
        System.out.println("Điểm A3 : " +this.da3);
        System.out.println("Điểm nguyên lý : "+ this.ngly1);
        System.out.println("ĐTB : " + Tinhdtb());
        System.out.println("Tiền nộp thi lại : " + ktra());
    }
    public double Tinhdtb(){
         float diemTB = (da1+da3+ngly1)/3;
         return diemTB;
    }
    public int ktra(){
        int dem = 0;
        int tien = 0;
        if(da1 < 5)
            dem++;
        if(da3 < 5 )
            dem++;
        if(ngly1 < 5)
            dem ++;
        tien = 90 * dem;
        return tien;
    }


}
