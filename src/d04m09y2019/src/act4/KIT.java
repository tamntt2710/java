package act4;

import java.util.Scanner;

public class KIT
{
    String mssv;
    String name;
    String sex;
    int namsinh;
    String team;
    String chucvu;

    public KIT(String mssv, String name, String sex, int namsinh, String team, String chucvu) {
        this.mssv = mssv;
        this.name = name;
        this.sex = sex;
        this.namsinh = namsinh;
        this.team = team;
        this.chucvu = chucvu;
    }
    public KIT(){

    }

    public String getMssv() {
        return mssv;
    }

    public void setMssv(String mssv) {
        this.mssv = mssv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getNamsinh() {
        return namsinh;
    }

    public void setNamsinh(int namsinh) {
        this.namsinh = namsinh;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getChucvu() {
        return chucvu;
    }

    public void setChucvu(String chucvu) {
        this.chucvu = chucvu;
    }
    public void NhapTT(){
        System.out.println("Nhập thông tin sinh viên : ");
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập mssv : ");
        mssv = sc.nextLine();
        System.out.println("Nhập họ và tên : ");
        name = sc.nextLine();
        System.out.println("Nhập giới tính : ");
        sex = sc.nextLine();
        System.out.println("Nhập năm sinh : ");
        namsinh = sc.nextInt();
        System.out.println("Nhập team : ");
        team = sc.nextLine();
        System.out.println("Nhập chức vụ : ");
        chucvu = sc.nextLine();

    }
    public void HienThiTT(){
        System.out.println(this.mssv + "-" + this.name + "-"+this.sex+"-"+this.namsinh+"-"+this.team+"-"+this.chucvu);
    }
    public void girl(){
        if(this.sex == "nữ"){
            if( 2019 - this.namsinh >= 18 ){
                System.out.println(this.mssv + "-" + this.name + "-"+this.sex+"-"+this.namsinh+"-"+this.team+"-"+this.chucvu);
            }
        }
    }
    public void teamweb(){
        if(this.team == "web") {
            System.out.println(this.mssv + "-" + this.name + "-" + this.sex + "-" + this.namsinh + "-" + this.team + "-" + this.chucvu);
        }
    }
    public void teamarduino(){
        if(this.team == "arduino"){
            System.out.println(this.mssv + "-" + this.name + "-" + this.sex + "-" + this.namsinh + "-" + this.team + "-" + this.chucvu);
        }

    }
    public void KIT_2000(){
        if(this.namsinh == 2000){
            System.out.println(this.mssv + "-" + this.name + "-" + this.sex + "-" + this.namsinh + "-" + this.team + "-" + this.chucvu);
        }
    }

}
