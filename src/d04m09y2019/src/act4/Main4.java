package act4;

import java.util.Scanner;

public class Main4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập số thành viên của KIT : ");
        int n = sc.nextInt();
        KIT[] kit = new KIT[n];
        int chon = 1;
        int Nhap ;
        while(chon == 1){
            System.out.println("***************************MENU*************************");
            System.out.println("1. Lập danh sách thành viên kit");
            System.out.println("2. Hiển thị danh sách thành viên");
            System.out.println("3. Hiển thị danh sách các bạn nữ đủ tuổi lấy chồng");
            System.out.println("4. Hiển thị danh sách các thành viên của Web/ Arduino.");
            System.out.println("5. Thống kê số thành viên KIT sinh năm 2000");
            System.out.println("6. Thoát");
            System.out.println("Nhập lựa chọn : ");
            Nhap = sc.nextInt();
            switch(Nhap){
                case 1 :
                    System.out.println("1. Lập danh sách thành viên KIT");
                    for(int i = 0 ;i < n ;i++){
                        kit[i].NhapTT();
                    }
                    break;
                case 2 :
                    System.out.println("2. Hiển thị danh sách thành viên");
                    for(int i = 0 ;i < n ;i++){
                        kit[i].HienThiTT();
                    }
                    break;
                case 3 :
                    System.out.println("3. Hiển thị danh sách các bạn nữ đủ tuổi lấy chồng");
                    for(int i = 0 ;i <n;i++){
                        kit[i].girl();
                    }
                    break;
                case 4 :
                    System.out.println("4. Hiển thị danh sách các thành viên của Web/ Arduino.");
                    for(int i = 0 ;i <n;i++){
                        kit[i].teamweb();
                    }
                    for(int i = 0 ;i <n ;i++){
                        kit[i].teamarduino();
                    }
                    break;
                case 5 :
                    System.out.println("5. Thống kê số thành viên kit sinh năm 2000");
                    for(int i = 0 ; i< n ;i++){
                        kit[i].KIT_2000();
                    }
                    break;
                case 6 :
                    chon = 0;
                    System.out.println("6. Thoát");
            }

        }
    }
}
