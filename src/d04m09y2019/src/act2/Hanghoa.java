package act2;

import java.util.Scanner;

public class Hanghoa {
    String msp;
    String ten;
    float giamua;
    float giaban;
    int sl;

    public Hanghoa(String msp, String ten, float giamua, float giaban, int sl) {
        this.msp = msp;
        this.ten = ten;
        this.giamua = giamua;
        this.giaban = giaban;
        this.sl = sl;
    }

    public Hanghoa() {

    }

    public String getMsp() {
        return msp;
    }

    public void setMsp(String msp) {
        this.msp = msp;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public float getGiamua() {
        return giamua;
    }

    public void setGiamua(float giamua) {
        this.giamua = giamua;
    }

    public float getGiaban() {
        return giaban;
    }

    public void setGiaban(float giaban) {
        this.giaban = giaban;
    }

    public int getSl() {
        return sl;
    }

    public void setSl(int sl) {
        this.sl = sl;
    }

    public void NhapTT(){
        System.out.println("Nhap mã sp : ");
        Scanner sc = new Scanner(System.in);
        msp = sc.nextLine();
        System.out.println("Nhập tên: ");
        ten = sc.nextLine();
        System.out.println("Nhap gia mua : ");
        giamua = sc.nextFloat();
        System.out.println("Nhap gia bán  : ");
        giaban = sc.nextFloat();
        System.out.println("Nhập số lượng : ");
        sl = sc.nextInt();
    }

    public float  Lai(){
        float tien = (giaban - giamua) * sl;
        return tien;
    }
    public void hienthi(){
        System.out.println("Tiền lãi/Lỗ là  : " + Lai());
    }
    public float Laitien(){
        float tien = (giaban * 2/3 * sl - giamua *1/3 *sl);
        return tien;
    }
}
