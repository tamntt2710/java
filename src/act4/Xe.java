package act4;

import java.util.Scanner;

public class Xe {
    public String Msc;
    public String tentaixe;
    public String soxe;
    public int doanhthu;

    public Xe() {
    }

    public Xe(String msc, String tentaixe, String soxe, int doanhthu) {
        Msc = msc;
        this.tentaixe = tentaixe;
        this.soxe = soxe;
        this.doanhthu = doanhthu;
    }

    public String getMsc() {
        return Msc;
    }

    public void setMsc(String msc) {
        Msc = msc;
    }

    public String getTentaixe() {
        return tentaixe;
    }

    public void setTentaixe(String tentaixe) {
        this.tentaixe = tentaixe;
    }

    public String getSoxe() {
        return soxe;
    }

    public void setSoxe(String soxe) {
        this.soxe = soxe;
    }

    public int getDoanhthu() {
        return doanhthu;
    }

    public void setDoanhthu(int doanhthu) {
        this.doanhthu = doanhthu;
    }

    public void NhapTT() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap ma so chuyen : ");
        Msc = scanner.nextLine();
        System.out.println("Nhap ho ten tai xe : ");
        tentaixe = scanner.nextLine();
        System.out.println("So xe : ");
        soxe = scanner.nextLine();
        System.out.println("Doanh thu : ");
        doanhthu = scanner.nextInt();
        scanner.nextLine();
    }

    public void showInfor() {
        System.out.println("Ma so chuyen  : " + Msc);
        System.out.println("Ten tai xe  : " + this.tentaixe);
        System.out.println("So xe  : " + this.soxe);
        System.out.println("Doanh tuh : " + doanhthu);
    }
}
