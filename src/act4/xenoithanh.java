package act4;

import java.util.Scanner;

public class xenoithanh extends Xe {
    public int sotuyen;
    public int sokm;

    public xenoithanh(String msc, String tentaixe, String soxe, int doanhthu, int sotuyen, int sokm) {
        super(msc, tentaixe, soxe, doanhthu);
        this.sotuyen = sotuyen;
        this.sokm = sokm;
    }

    public xenoithanh() {

    }

    public int getSotuyen() {
        return sotuyen;
    }

    public void setSotuyen(int sotuyen) {
        this.sotuyen = sotuyen;
    }

    public int getSokm() {
        return sokm;
    }

    public void setSokm(int sokm) {
        this.sokm = sokm;
    }

    @Override
    public void NhapTT() {
        super.NhapTT();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap so tuyen : ");
        sotuyen = scanner.nextInt();
        System.out.println("So km di dc : ");
        sokm = scanner.nextInt();
    }

    public void showInfor() {
        super.showInfor();
        System.out.println("So tuyen : " + this.sotuyen);
        System.out.println("So km di duoc : " + this.sokm);
    }
}
