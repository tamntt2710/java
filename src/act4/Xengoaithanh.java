package act4;

import java.util.Scanner;

public class Xengoaithanh extends Xe {
    public String noiden;
    public String songay;

    public Xengoaithanh(String msc, String tentaixe, String soxe, int doanhthu, String noiden, String songay) {
        super(msc, tentaixe, soxe, doanhthu);
        this.noiden = noiden;
        this.songay = songay;
    }

    public Xengoaithanh() {

    }

    public String getNoiden() {
        return noiden;
    }

    public void setNoiden(String noiden) {
        this.noiden = noiden;
    }

    public String getSongay() {
        return songay;
    }

    public void setSongay(String songay) {
        this.songay = songay;
    }

    @Override
    public void NhapTT() {
        super.NhapTT();
        System.out.println("Nhap noi den : ");
        Scanner scanner = new Scanner(System.in);
        noiden = scanner.nextLine();
        System.out.println("Nhap so ngay : ");
        songay = scanner.nextLine();
    }

    public void showInfor() {
        super.showInfor();
        System.out.println("Noi den  : " + this.noiden);
        System.out.println("So ngay : " + this.songay);
    }
}
