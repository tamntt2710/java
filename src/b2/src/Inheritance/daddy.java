package Inheritance;

public class daddy {
    public static void main(String[] args) {
        /*
        Student student = new Student();
        Employee employee = new Employee();
        student.getFullName();
        employee.getFullName();

         */


       Employee employee = new Employee("Nguyen Thi Thanh Tam","0123",3000,"123D-123E","Mânger");
       Student student = new Student("Nguyen Thi Ngọc Bich","012322","E123D","FTU",3.14f);


        Person person = new Person("Ngueyn Van Thang","0187");


//        System.out.println("Thong tin person : ");
//        person.showInfor();
//        System.out.println();
//
//
//        System.out.println("Thong tin student ");
//        student.showInfor();
//        System.out.println();
//
//        System.out.println("Thong tin employee : ");
//        employee.showInfor();
//        System.out.println();

    Person[] people = new Person[3];
    people[0] = person;
    people[1] = student;
    people[2] = employee;


    for(Person p : people){
        System.out.println("Thong tin : ");
        p.showInfor();
        System.out.println();
    }

    }

}
