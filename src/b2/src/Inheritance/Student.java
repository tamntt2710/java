package Inheritance;

public class Student extends Person {
    private String studentID;
    private String univerName;
    private float argMark;

    public Student(String studentID, String univerName, float argMark) {
        this.studentID = studentID;
        this.univerName = univerName;
        this.argMark = argMark;
    }

    public Student(String fullName, String studentID, String univerName, float argMark) {
        super(fullName);
        this.studentID = studentID;
        this.univerName = univerName;
        this.argMark = argMark;
    }

    public Student(String fullName, String ID, String studentID, String univerName, float argMark) {
        //super(fullName, ID);
        this.studentID = studentID;
        this.univerName = univerName;
        this.argMark = argMark;
    }

    public Student() {

    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getUniverName() {
        return univerName;
    }

    public void setUniverName(String univerName) {
        this.univerName = univerName;
    }

    public float getArgMark() {
        return argMark;
    }

    public void setArgMark(float argMark) {
        this.argMark = argMark;
    }

    @Override
    public void showInfor() {
        super.showInfor();
        System.out.println("Student ID  : " + studentID);
        System.out.println("University : " + univerName);
        System.out.println("Averger Mark : " + argMark);
    }
}

//    @Override
//    public boolean equals(Object otherobject) {
//        if(!= super.equals(otherobject){
//            return false;
//        }
//        else

