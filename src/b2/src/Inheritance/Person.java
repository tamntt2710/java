package Inheritance;

public class Person {
    private String fullName;
    private String ID;

    public Person(){

    }

    public Person(String fullName) {
        this.fullName = fullName;
    }

    public Person(String fullName, String ID) {
        this.fullName = fullName;
        this.ID = ID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
    public void showInfor(){
        String infor = "Full Name : " + fullName +"\nID: " + getID();
        System.out.println(infor);
    }
}
