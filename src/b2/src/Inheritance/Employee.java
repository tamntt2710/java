package Inheritance;

public class Employee extends Person{
    private float sallary;
    private String eID;
    private String role;

    public Employee(){

    }


    public Employee(String fullName, String ID, float sallary, String eID, String role) {
        super(fullName, ID);
        this.sallary = sallary;
        this.eID = eID;
        this.role = role;
    }

    public float getSallary() {
        return sallary;
    }

    public void setSallary(float sallary) {
        this.sallary = sallary;
    }

    public String geteID() {
        return eID;
    }

    public void seteID(String eID) {
        this.eID = eID;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public void showInfor() {
        super.showInfor();
        System.out.println("sallary : "+ sallary);
        System.out.println("Employee ID : "+eID);
        System.out.println("Employee role : "+ role);
    }
}
