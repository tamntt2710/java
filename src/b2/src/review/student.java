package review;

public class student extends people {
    private String sID;
    private String university;
    private float mark;

    public String getsID() {
        return sID;
    }

    public void setsID(String sID) {
        this.sID = sID;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public student(String name, String ID, String sID, String university, float mark) {
        super(name, ID);
        this.sID = sID;
        this.university = university;
        this.mark = mark;
    }
    @Override
    public void showInfor(){
       super.showInfor();
        System.out.println("university : " + university);
        System.out.println("sID" + sID);
        System.out.println("mark  : "+ mark);
    }

}

