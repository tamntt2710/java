package regularexpressions;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email {
    /*
    Sử dụng regular Expresstion để :
    - Kiểm tra định dạng email
    ví dụ : tranhung3343@xmail.com
     */

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        String regex = "^[a-zA-Z]+[a-zA-Z0-9]*@[a-zA-Z]+mail.com$";//bắt đầu bằng chữ cái -> số
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()){
            System.out.println("Hop le");
        }else {
            System.out.println("Vui long nhap lai!");
        }

    }
}
