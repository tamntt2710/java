package regularexpressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class find_replace {
    public static void main(String[] args) {

        String input = " Hello      abc    xyz    good night !    ";
        String regex = "[\\s]+"; // thay thế cụm dấu cách dùng regular expresstion
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if(matcher.find()){
            System.out.println(matcher.replaceAll("_"));
        }
    }
/*
        String[] fullName = {"Nguyen Thi Thanh Tam ", "Nguyen Thi Ngoc Bich","Nguyen Ngoc Diep"};
        String regex = "^Nguyen.*" ;//  tìm kiếm gần đúng
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher;
        for(int i=0;i<fullName.length;i++){
            matcher = pattern.matcher(fullName[i]);
            if(matcher.find()){
                System.out.println(fullName[i]);
            }
        }

    }

 */

}
