package regularexpressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class mssv {
    /*
    Sử dụng regular expresstions để :
    - Ktra mssv
     */
    public static void main(String[] args) {
        String input = "C12DCkQ232";
        String regex = "^[bBcC]{1}\\d{2}[đCnnNqQtTsSkD]{4}\\d{3}$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher=pattern.matcher(input);
        if(matcher.find()){
            System.out.println("OK");
        }else
        {
            System.out.println("Nhap lai mssv");
        }
    }
}
