package regularexpressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class telephone_number {
    public static void main(String[] args) {
        String phoneNumber = "(030).2334.5555";
        String input = "0929197341";

        String regex ="\\(\\d{3}\\).\\d{4}.\\d{4}$";//0[98]{1}\d{8}$
        Pattern pattern= Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phoneNumber);
        if(matcher.find()){
            System.out.println("OK");
        }else{
            System.out.println("KO HOP LE");
        }
    }
}
