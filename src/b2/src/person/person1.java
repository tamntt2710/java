package person;

public class person1 {

    private String name;
    private String fullName;
    private int age;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.fullName = name;
        splitName();
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private void splitName(){

        String[] names = fullName.split(" ");
        System.out.println("First name  "+ names[0] + "\n" +"Secondname  " + names[1]);

    }
}
