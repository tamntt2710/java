package topica.edu.vn;

public class NhanVien implements  Comparable<NhanVien>{
    public String Name;
    public int snv;

    public NhanVien(String name, int snv) {
        Name = name;
        this.snv = snv;
    }

    public NhanVien() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getSnv() {
        return snv;
    }

    public void setSnv(int snv) {
        this.snv = snv;
    }

    @Override
    public int compareTo(NhanVien o) { // tra ve gia tri = 0 neu bang nhau , >1 neu doi tuong 1>2

        int ssten = this.Name.compareToIgnoreCase(o.getName());
        if(ssten == 0){
            if(this.snv == o.snv)
                return 0;
            if(this.snv > o.snv)
                return -1;
            return 1;
        }
        return ssten;

    }

}

