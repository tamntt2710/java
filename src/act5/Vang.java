package act5;

import java.util.Scanner;

public class Vang extends GiaoDich {
    public String loaivang;

    public Vang(String mdd, int ngay, int thang, int nam, int dongia, int soluong, String loaivang) {
        super(mdd, ngay, thang, nam, dongia, soluong);
        this.loaivang = loaivang;
    }

    public Vang() {

    }

    public String getLoaivang() {
        return loaivang;
    }

    public void setLoaivang(String loaivang) {
        this.loaivang = loaivang;
    }

    @Override
    public void NhapTT() {
        super.NhapTT();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap loai vang : ");
        loaivang = scanner.nextLine();
    }

    public int thanhtien() {
        super.thanhtien();
        return thanhtien();
    }

    public void showInfor() {
        super.showInfor();
        System.out.println("Loai vang : " + loaivang);
    }
}
