package act5;

import java.util.Scanner;

public class Tien extends GiaoDich {
    private float tigia;
    private String Loaitien;

    public Tien(String mdd, int ngay, int thang, int nam, int dongia, int soluong, float tigia, String loaitien) {
        super(mdd, ngay, thang, nam, dongia, soluong);
        this.tigia = tigia;
        Loaitien = loaitien;
    }

    public Tien() {

    }

    public float getTigia() {
        return tigia;
    }

    public void setTigia(float tigia) {
        this.tigia = tigia;
    }

    public String getLoaitien() {
        return Loaitien;
    }

    public void setLoaitien(String loaitien) {
        Loaitien = loaitien;
    }

    public float tinhtien() {
        float thanhtien;
        thanhtien = getSoluong() * getDongia() * tigia;
        return thanhtien;
    }

    @Override
    public void NhapTT() {
        super.NhapTT();
        System.out.println("Nhap ti gia : ");
        Scanner scanner = new Scanner(System.in);
        tigia = scanner.nextFloat();
        System.out.println("Nhap loai tien: ");
        Loaitien = scanner.nextLine();
    }

    public void showInfor() {
        super.showInfor();
        System.out.println("Ti gia : " + tigia);
        System.out.println("Loai tien: " + Loaitien);

    }
}
