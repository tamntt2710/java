package act5;

import java.util.Scanner;

public class GiaoDich {
    private String mdd;
    private int ngay;
    private int thang;
    private int nam;
    private int dongia;
    private int soluong;

    public GiaoDich(String mdd, int ngay, int thang, int nam, int dongia, int soluong) {
        this.mdd = mdd;
        this.ngay = ngay;
        this.thang = thang;
        this.nam = nam;
        this.dongia = dongia;
        this.soluong = soluong;
    }

    public GiaoDich() {

    }

    public String getMdd() {
        return mdd;
    }

    public void setMdd(String mdd) {
        this.mdd = mdd;
    }

    public int getNgay() {
        return ngay;
    }

    public void setNgay(int ngay) {
        this.ngay = ngay;
    }

    public int getThang() {
        return thang;
    }

    public void setThang(int thang) {
        this.thang = thang;
    }

    public int getNam() {
        return nam;
    }

    public void setNam(int nam) {
        this.nam = nam;
    }

    public int getDongia() {
        return dongia;
    }

    public void setDongia(int dongia) {
        this.dongia = dongia;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public void NhapTT() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap ma giao dich : ");
        mdd = scanner.nextLine();
        System.out.println("Nhap ngay thang nam giao dich : ");
        ngay = scanner.nextInt();
        thang = scanner.nextInt();
        nam = scanner.nextInt();
        System.out.println("Nhap don gia : ");
        dongia = scanner.nextInt();
        System.out.println("Nhap so luong : ");
        soluong = scanner.nextInt();
    }

    public int thanhtien() {
        int thanhtien;
        thanhtien = soluong * dongia;
        return thanhtien;
    }

    public void showInfor() {
        System.out.println("ma giao dich : " + mdd);
        System.out.println("Ngay thang naw giao dich : " + ngay + "/" + thang + "/" + nam);
        System.out.println("Nhap don gia : " + dongia);
        System.out.println("Nhap so luong : " + soluong);
        System.out.println("Thanh tien : " + thanhtien());
    }


}
