package act6;

import java.util.Scanner;

public class Customer extends Person {
    private String tencty;
    private String trigia;

    public Customer(String name, String address, String trigia) {
        super(name, address);
        this.tencty = tencty;
        this.trigia = trigia;
    }

    public Customer() {

    }

    public String getTencty() {
        return tencty;
    }

    public void setTencty(String tencty) {
        this.tencty = tencty;
    }

    public String getTrigia() {
        return trigia;
    }

    public void setTrigia(String trigia) {
        this.trigia = trigia;
    }

    @Override
    public void NhapTT() {
        super.NhapTT();
        System.out.println("Nhap ten cong ty : ");
        Scanner scanner = new Scanner(System.in);
        tencty = scanner.nextLine();
        System.out.println("Nhap tri gia hoa don : ");
        trigia = scanner.nextLine();
    }

    public void showInfor() {
        super.showInfor();
        System.out.println("Ten cty : " + tencty);
        System.out.println("Tri gia hoa don : " + trigia);
    }
}
