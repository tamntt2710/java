package act6;

import java.util.Scanner;

public class Student extends Person {
    private float sub1;
    private float sub2;
    private float dtb;

    public Student(String name, String address, float sub1, float sub2, float dtb) {
        super(name, address);
        this.sub1 = sub1;
        this.sub2 = sub2;
        this.dtb = dtb;
    }

    public Student() {

    }

    public float getSub1() {
        return sub1;
    }

    public void setSub1(float sub1) {
        this.sub1 = sub1;
    }

    public float getSub2() {
        return sub2;
    }

    public void setSub2(float sub2) {
        this.sub2 = sub2;
    }

    public float getDtb() {
        return dtb;
    }

    public void setDtb(float dtb) {
        this.dtb = dtb;
    }

    public float diemTB() {
        dtb = (sub1 + sub2) / 2;
        return dtb;
    }

    @Override
    public void NhapTT() {
        Scanner scanner = new Scanner(System.in);
        super.NhapTT();
        System.out.println("Nhap diem mon 1 : ");
        sub1 = scanner.nextFloat();
        System.out.println("Nhap diem mon 2 : ");
        sub2 = scanner.nextFloat();
    }

    @Override
    public void showInfor() {
        super.showInfor();
        System.out.println("Diem mon 1 : " + sub1);
        System.out.println("Diem mon 2: " + sub2);
        System.out.println("Diem tb :" + dtb);
    }
}
