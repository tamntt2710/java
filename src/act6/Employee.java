package act6;

import java.util.Scanner;

public class Employee extends Person {
    private float hsluong;
    private float tienluong;

    public Employee() {
    }

    public Employee(String name, String address, float hsluong, float tienluong) {
        super(name, address);
        this.hsluong = hsluong;
        this.tienluong = tienluong;
    }

    public Employee(float hsluong, float tienluong) {
        this.hsluong = hsluong;
        this.tienluong = tienluong;
    }

    public float getHsluong() {
        return hsluong;
    }

    public void setHsluong(float hsluong) {
        this.hsluong = hsluong;
    }

    public float getTienluong() {
        return tienluong;
    }

    public void setTienluong(float tienluong) {
        this.tienluong = tienluong;
    }

    public float tinhLuong() {
        tienluong += tienluong * hsluong;
        return tienluong;
    }

    @Override
    public void NhapTT() {
        super.NhapTT();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap luong : ");
        tienluong = scanner.nextFloat();
        System.out.println("Nhap he so luong : ");
        hsluong = scanner.nextFloat();
    }

    public void showInfor() {
        super.showInfor();
        System.out.println("TIen luong :" + tienluong);
    }
}
