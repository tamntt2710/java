package act6;

import java.util.Scanner;

public class Person {
    private String Name;
    private String address;

    public Person(String name, String address) {
        Name = name;
        this.address = address;
    }

    public Person() {

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void NhapTT() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap ten : ");
        Name = scanner.nextLine();
        System.out.println("Nhap dia chi : ");
        address = scanner.nextLine();
    }

    public void showInfor() {
        System.out.println("Ten : " + Name);
        System.out.println("Dia chi : " + address);
    }
}
