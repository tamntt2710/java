package act1;

import java.util.Scanner;

public class Vehicle {
    private int giatri;
    private int dungtich;
    private float thue;

    public Vehicle(int giatri, int dungtich) {
        this.giatri = giatri;
        this.dungtich = dungtich;
        this.thue = thue;
    }

    public Vehicle() {
    }

    public int getGiatri() {
        return giatri;
    }

    public void setGiatri(int giatri) {
        this.giatri = giatri;
    }

    public int getDungtich() {
        return dungtich;
    }

    public void setDungtich(int dungtich) {
        this.dungtich = dungtich;
    }

    public float getThue() {
        return thue;
    }

    public void setThue(float thue) {
        this.thue = thue;
    }

    public void nhapTT() {
        System.out.println("Nhap gia tri cua xe : ");
        Scanner scanner = new Scanner(System.in);
        giatri = scanner.nextInt();
        System.out.println("Nhap dung tich cua xe : ");
        dungtich = scanner.nextInt();
    }

    public float calculatot() {
        if (dungtich < 100) {
            thue = giatri / 100;
        } else if (100 < dungtich && dungtich < 200) {
            thue = (giatri * 3) / 100;
        } else thue = (giatri * 5) / 100;
        return thue;
    }

}
