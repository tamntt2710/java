package act7;

import java.util.Scanner;

public class vietnam extends khachhang {
    private String doituong;
    private int dinhmuc;
    public float thanhtien;

    public vietnam(String mkh, String hoten, int ngay, int thang, int nam, int soluong, float dongia, String doituong, int dinhmuc) {
        super(mkh, hoten, ngay, thang, nam, soluong, dongia);
        this.doituong = doituong;
        this.dinhmuc = dinhmuc;
    }

    public  vietnam(){

    }

    public String getDoituong() {
        return doituong;
    }

    public void setDoituong(String doituong) {
        this.doituong = doituong;
    }

    public int getDinhmuc() {
        return dinhmuc;
    }

    public void setDinhmuc(int dinhmuc) {
        this.dinhmuc = dinhmuc;
    }

    @Override
    public void nhapTT(){
        super.nhapTT();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap doi tuong khach hang : ");
        doituong = scanner.nextLine();
        System.out.println("Nhap dinh muc : ");
        dinhmuc= scanner.nextInt();
    }
    public float thanhtien(){
        if(getSoluong() <= getDinhmuc()){
            thanhtien = getSoluong() * getDongia();
        }

        else{
            thanhtien = (float) (getSoluong()*getDongia()*dinhmuc+ (getSoluong()-getDinhmuc())*getDongia()*2.5);
        }
        return thanhtien;
    }
    public void show(){
        System.out.println("ma khach hang : "+getMkh()+";ho ten : "+getHoten()+";ngay,thang,nam : "+getNgay()+getThang()+getNam()+";Đoi tuong : "+doituong+"so luong : "+getSoluong()+";đơn giá : " + getDongia()+";định mức : "+getDinhmuc());
    }

}
