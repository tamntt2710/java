package act7;

import java.util.Scanner;

public class khachhang {
    private String mkh;
    private String hoten;
    private int ngay;
    private int thang;
    private int nam;
    private int soluong;
    private float dongia;
    private float thanhtien;


    public khachhang(String mkh, String hoten, int ngay, int thang, int nam, int soluong, float dongia) {
        this.mkh = mkh;
        this.hoten = hoten;
        this.ngay = ngay;
        this.thang = thang;
        this.nam = nam;
        this.soluong = soluong;
        this.dongia = dongia;
    }
    public khachhang(){

    }


    public String getMkh() {
        return mkh;
    }

    public void setMkh(String mkh) {
        this.mkh = mkh;
    }

    public String getHoten() {
        return hoten;
    }

    public void setHoten(String hoten) {
        this.hoten = hoten;
    }

    public int getNgay() {
        return ngay;
    }

    public void setNgay(int ngay) {
        this.ngay = ngay;
    }

    public int getThang() {
        return thang;
    }

    public void setThang(int thang) {
        this.thang = thang;
    }

    public int getNam() {
        return nam;
    }

    public void setNam(int nam) {
        this.nam = nam;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }

    public float getDongia() {
        return dongia;
    }

    public void setDongia(float dongia) {
        this.dongia = dongia;
    }

    public void nhapTT(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap ma khach hang : ");
        mkh = scanner.nextLine();
        System.out.println("Nhap ho ten khach hang : ");
        hoten = scanner.nextLine();
        System.out.println("Nhap ngay : ");
        ngay = scanner.nextInt();
        System.out.println("Nhap thang : ");
        thang = scanner.nextInt();
        System.out.println("Nhap nam : ");
        nam = scanner.nextInt();
        System.out.println("Nhap so luong : ");
        soluong= scanner.nextInt();
        System.out.println("Nhap don gia : ");
        dongia = scanner.nextFloat();
    }

}
