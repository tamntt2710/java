package d30m10y2019.bai6_arraylist;

public class person {
    // Lớp Person: bao gồm các thuộc tính họ tên, địa chỉ, phương thức toString.
    private String fulName;
    private String address;

    public person(String fulName, String address) {
        this.fulName = fulName;
        this.address = address;
    }
    public person(){

    }

    public String getFulName() {
        return fulName;
    }

    public void setFulName(String fulName) {
        this.fulName = fulName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String toString(){
        return ("Name : " + getFulName() + "\t" +"Address : " + getAddress());
    }
}
