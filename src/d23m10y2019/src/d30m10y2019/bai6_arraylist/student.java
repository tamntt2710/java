package d30m10y2019.bai6_arraylist;

public class student extends person {
    //o Lớp Student: bao gồm các thuộc tính điểm môn học 1, điểm môn học 2, và các phương thức:
    // tính điểm TB, đánh giá, overriding phương thức toString trả về bảng điểm sinh viên (gồm thông tin thuộc tính và điểm TB).
    private float point1;
    private float point2;

    public student(String fulName, String address, float point1, float point2) {
        super(fulName, address);
        this.point1 = point1;
        this.point2 = point2;
    }

    public student(float point1, float point2) {
        this.point1 = point1;
        this.point2 = point2;
    }

    public student(){

    }

    public float dtb(){
        float dtb = (point1 + point2) / 2;
        return dtb;
    }
    @Override
    public String toString(){
        super.toString();
        return ("Name : "+getFulName()+"\t"+"Address : "+getAddress()+"\t"+"diểm trung bình : "+dtb());
    }
}
