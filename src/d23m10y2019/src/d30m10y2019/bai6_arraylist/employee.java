package d30m10y2019.bai6_arraylist;

import java.util.Scanner;

public class employee extends person{
    //o Lớp Employee: bao gồm thuộc tính heSoLương, và các phương thức:
    // tính lương, đánh giá, overriding phương thức toString trả về bảng lương cho nhân viên (gồm thông tin thuộc tính đối tượng và tiền lương).
    private float hesoluong;

    public employee(String fulName, String address, float hesoluong) {
        super(fulName, address);
        this.hesoluong = hesoluong;
    }

    public employee(float hesoluong) {
        this.hesoluong = hesoluong;
    }
    public employee(){

    }

    public float getHesoluong() {
        return hesoluong;
    }

    public void setHesoluong(float hesoluong) {
        this.hesoluong = hesoluong;
    }
    public float tinhluong(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập lương : ");
        float luong = scanner.nextFloat();
        luong = luong+luong*getHesoluong();
        return luong;
    }
    @Override
    public String toString(){
        super.toString();
        return ("Name : "+getFulName()+"\t"+"Address : "+getAddress()+"\t"+"Luong: "+tinhluong());
    }

}
