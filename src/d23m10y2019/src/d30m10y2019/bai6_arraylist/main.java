package d30m10y2019.bai6_arraylist;

import java.util.ArrayList;

public class main {
    public static void main(String[] args) {
        person person = new person("Nguyễn Tâm","Nam định");
        student student = new student("nguyễn tâm","nam định",8.9f,8.1f);
        employee employee = new employee("nguyễn tâm","nam định",0.5f);
        customer customer = new customer("nguyễn bích","hà nội","ABC",12000.3f);
        ArrayList mList = new ArrayList();
        mList.add(person);
        mList.add(student);
        mList.add(employee);
        mList.add(customer);

        mList.forEach((obj)->{
            System.out.println(obj);
        });
    }
}
