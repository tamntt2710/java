package d30m10y2019.bai6_arraylist;

public class customer extends person {
    //o Lớp Customer: bao gồm thuộc tính tên công ty,trị giá hóa đơn,
    // đánh giá, và phương thức toString trả về thông tin hóa đơn cho khách hàng (gồm các thuộc tính của đối tượng).

    private String tencty;
    private float gtrihoadon;

    public customer(String fulName, String address, String tencty, float gtrihoadon) {
        super(fulName, address);
        this.tencty = tencty;
        this.gtrihoadon = gtrihoadon;
    }

    public customer(String tencty, float gtrihoadon) {
        this.tencty = tencty;
        this.gtrihoadon = gtrihoadon;
    }

    public customer(){

    }

    public String getTencty() {
        return tencty;
    }

    public void setTencty(String tencty) {
        this.tencty = tencty;
    }

    public float getGtrihoadon() {
        return gtrihoadon;
    }

    public void setGtrihoadon(float gtrihoadon) {
        this.gtrihoadon = gtrihoadon;
    }

    @Override
    public String toString(){
        super.toString();
        return ("Name : " +getFulName()+"Address: "+getAddress()+"tên cty : "+getTencty()+"\n"+"giá trị hóa đơn : "+getGtrihoadon());
    }
}
