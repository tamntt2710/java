package d30m10y2019;

import java.util.ArrayList;

public class List {
    public static void main(String[] args) {
        Student st1 = new Student("Nguyễn Thị Thanh Tâm",18,"nữ");
        Student st2 = new Student("Nguyễn Thị Ngọc Bích",16,"nữ");
        ArrayList<Student> students0 = new ArrayList<>();
        students0.add(st1);
        students0.add(st2);
        students0.forEach((obj)-> System.out.println(obj.getFulName()));
    }
}
