package d30m10y2019;

public class Student {
    private String fulName;
    private int age;
    private String gender;

    public Student(String fulName, int age, String gender) {
        this.fulName = fulName;
        this.age = age;
        this.gender = gender;
    }
    public Student(){

    }

    public String getFulName() {
        return fulName;
    }

    public void setFulName(String fulName) {
        this.fulName = fulName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
