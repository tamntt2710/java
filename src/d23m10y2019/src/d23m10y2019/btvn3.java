package d23m10y2019;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class btvn3 {
    public static void menu(){
        System.out.println("1. Lọc ra các từ có ít nhất 3 từ trở lên.");
        System.out.println("2. Sắp xếp lại các từ Theo thứ tự A-Z");
        System.out.println("3. Nhập vào 1 từ bất kì. Kiểm tra xem từ ấy có trong danh sách hay không?");
        System.out.println("4. Thoát");
    }
    public static void input(ArrayList<String> mList){
        int chon = 1;
        Scanner sc = new Scanner(System.in);
        do{
            System.out.println("Nhap : ");
            String a = sc.nextLine();
            sc.nextLine();
            mList.add(a);
            System.out.println("Bạn có muốn nhập nữa ?" );
            chon = sc.nextInt();
        }while (chon != 0);
    }
    public static void sort(ArrayList mList){
        mList.forEach((obj)->{
            String arr[] = obj.toString().split(" ");
            if(arr.length >= 3){
                System.out.println(obj);
            }
        }
        );
    }

    public static void sx(ArrayList mList){
        Collections.sort(mList);
        for(int i = 0 ;i < mList.size();i++){
            System.out.println("\t"+mList.get(i));
        }
    }
    public static void ktra(ArrayList mList){
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập từ cần ktra : ");
        String str = sc.nextLine();
        System.out.println("Ktra : " + mList.contains(str));
    }

    public static void main(String[] args) {
        int daNhap = 1;
        ArrayList mList = new ArrayList();
        input(mList);
        while (daNhap == 1){
            menu();
            System.out.println("Nhập lựa chọn của bạn : ");
            Scanner sc = new Scanner(System.in);
            int a = sc.nextInt();
            switch (a){
                case 1:  System.out.println("1. Lọc ra các từ có ít nhất 3 từ trở lên.");
                sort(mList);
                break;
                case 2: System.out.println("2. Sắp xếp lại các từ Theo thứ tự A-Z");
                sx(mList);
                break;
                case 3: System.out.println("3. Nhập vào 1 từ bất kì. Kiểm tra xem từ ấy có trong danh sách hay không?");
                ktra(mList);
                break;
                case 4:  System.out.println("4. Thoát");
                daNhap = 0;
            }
        }
    }
}
