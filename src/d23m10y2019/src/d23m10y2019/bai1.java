package d23m10y2019;
/*Nhập vào danh sách các số nguyên dương
* 1.In ra màn hình danh sách các số chẵn
* 2.In ra màn hình danh sách các số nguyên tố
* 3.Nhập vào từ bàn phím 1 số nguyên tố bất kì .Kiếm tra xem số đó có nằm trong danh sách ban đầu hay không ?*/

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class bai1 {
    public int ktrachan(int n) {
        int ktra = 1;
        if (n % 2 == 0) {
            ktra = 0;
        } else
            ktra = 1;
        return ktra;
    }

    public int ktraSNT(int n) {
        int nt = 1;
        if (n == 1 || n == 2) {
            nt = 0;
        } else {
            for (int i = 2; i < n / 2; i++) {
                if (n % i == 0) {
                    nt = 1;
                } else
                    nt = 0;
            }
        }
        return nt;
    }

    public static void main(String[] args) {
        List<Integer> mList = new ArrayList<Integer>();
        System.out.println("Nhap danh sách các số nguyên : ");
        int chon = 1;
        do {
            int x;
            Scanner scanner = new Scanner(System.in);
            System.out.println("Nhap : ");
            x = scanner.nextInt();
            mList.add(x);
            System.out.println("Ban co muon nhập nữa không ? ");
            chon = scanner.nextInt();
        } while (chon == 0);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập 1 số bất kì : ");
        for (int i = 0; i < mList.size(); i++) {
            System.out.println(i + "\t" + mList.get(i));
        }

        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i) % 2 == 0) {
                System.out.println(i + "\t" + mList.get(i));
            }
        }
        int a = scanner.nextInt();
        for (int i = 0; i < mList.size(); i++) {
            System.out.println("Kiểm tra " + mList.contains(a));
        }
    }
}