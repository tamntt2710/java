package d23m10y2019;

import java.util.*;

public class btvn1 {
    public static void sortInt(ArrayList<Integer> mList, int order) {
        if (order == 1) {
            Collections.sort(mList);
        } else if (order == 2) {
            Collections.sort(mList, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o2 - o1;
                }
            });
        }
    }

    public static void menu() {
        System.out.println("1. Sắp xếp dãy Theo thứ tự tăng dần.");
        System.out.println("2. Sắp xếp lại dãy Theo thứ tự giảm dần.");
        System.out.println("3. Sắp xếp lại dãy ban đầu sao cho 2 số chẵn ko đứng cạnh nhau, 2 số lẻ ko đứng cạnh nhau. (Nếu còn toàn chẵn hoặc toàn lẻ thì chèn số 0 vào giữa)");
        System.out.println("4. Nhập vào từ bàn phím 1 số nguyên. Kiếm tra số đó có tồn tại trong dãy ko và xuất hiện bao nhiêu lần.\n");
        System.out.println("5. Thoát");
    }

    public static void input(ArrayList<Integer> mList) {

        System.out.println("Nhap danh sách các số nguyên : ");
        int chon = 1;
        do {
            int x;
            Scanner scanner = new Scanner(System.in);
            System.out.println("Nhap : ");
            x = scanner.nextInt();
            mList.add(x);
            System.out.println("Ban co muon nhập nữa không ? ");
            chon = scanner.nextInt();
        } while (chon != 0);
    }

    public static void output(ArrayList<Integer> mList) {
        for (int i = 0; i < mList.size(); i++) {
            System.out.println("\t" + mList.get(i));
        }
    }

    public static void ktra(ArrayList<Integer> mList) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập 1 số bất kì : ");
        int a = scanner.nextInt();
        System.out.println("Kiểm tra " + mList.contains(a));
    }

//    public static ArrayList chanLe(ArrayList<Integer> a) {
//        ArrayList evenList = new ArrayList<Integer>();
//        ArrayList oddList = new ArrayList<Integer>();
//        a.forEach((obj) -> {
//                    if (obj % 2 == 0) {
//                        evenList.add(obj);
//                    } else
//                        oddList.add(obj);
//                }
//        );
//        System.out.println("EVEN : ");
//        evenList.forEach((obj) -> {
//            System.out.println("\t" + obj);
//        });
//        System.out.println("ODD : ");
//        oddList.forEach((obj) -> {
//            System.out.println("\t" + obj);
//        });
//    }

        public static void main (String[]args){
            int daNhap = 1;
            ArrayList mList = new ArrayList<Integer>();
            input(mList);
            while (daNhap == 1) {
                menu();
                System.out.println("Mời bạn lựa chọn : ");
                Scanner sc = new Scanner(System.in);
                int a = sc.nextInt();
                switch (a) {
                    case 1:  System.out.println("1. Sắp xếp dãy Theo thứ tự tăng dần.");
                    sortInt(mList,1);
                    output(mList);
                    break;
                    case 2:System.out.println("2. Sắp xếp lại dãy Theo thứ tự giảm dần.");
                    sortInt(mList,2);
                    output(mList);
                    break;
                    case 3: System.out.println("3. Sắp xếp lại dãy ban đầu sao cho 2 số chẵn ko đứng cạnh nhau, 2 số lẻ ko đứng cạnh nhau. (Nếu còn toàn chẵn hoặc toàn lẻ thì chèn số 0 vào giua");
                    break;
                    case 4: System.out.println("4. Nhập vào từ bàn phím 1 số nguyên. Kiếm tra số đó có tồn tại trong dãy ko và xuất hiện bao nhiêu lần.\n");
                    ktra(mList);
                    break;
                    case 5 : System.out.println("5. Thoát");
                    daNhap = 0;
                }
            }
        }

    }


