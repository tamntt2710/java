package Extends;

public class SmartPhone extends Phone {
    private int camera;
    private String security;

    public SmartPhone(int camera, String security) {
        this.camera = camera;
        this.security = security;
    }

    public SmartPhone(String name, String display, int battery, int camera, String security) {
        super(name, display, battery);
        this.camera = camera;
        this.security = security;
    }

    public SmartPhone(String name, String display, int camera, String security) {
        super(name, display);
        this.camera = camera;
        this.security = security;
    }

    public int getCamera() {
        return camera;
    }

    public void setCamera(int camera) {
        this.camera = camera;
    }

    public String getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        this.security = security;
    }
    @Override
    public void showInfor(){
        System.out.println("Name : " + getName());
        System.out.println("camera: " + this.camera );
        System.out.println("security : "+this.security);
    }
}
