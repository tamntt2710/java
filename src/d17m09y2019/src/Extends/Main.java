package Extends;

public class Main {
    public static void main(String[] args) {
        Phone phone = new Phone("iphone","5,5inch",3000);
        System.out.println(phone.getName());
        SmartPhone smartPhone = new SmartPhone("samsung","4.5inch",3000,4,"averger");
        System.out.println(smartPhone.getName());
        smartPhone.showInfor();
    }

}
