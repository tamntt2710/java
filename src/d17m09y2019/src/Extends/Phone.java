package Extends;

public class Phone {
    private String name;
    private String display;
    private int battery;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public Phone() {
    }

    public Phone(String name, String display, int battery) {
        this.name = name;
        this.display = display;
        this.battery = battery;
    }

    public Phone(String name, String display) {
        this.name = name;
        this.display = display;
    }
    public void showInfor(){
        System.out.println("Name : " + this.name);

    }
}
