package bai5;

import java.util.Scanner;

public class Vang extends giaodich{
    private String loaivang;

    public Vang(String mgd, int ngaygd, int thanggd, int namgd, float dongia, int soluong, String loaivang) {
        super(mgd, ngaygd, thanggd, namgd, dongia, soluong);
        this.loaivang = loaivang;
    }

    public Vang() {
    }

    public String getLoaivang() {
        return loaivang;
    }

    public void setLoaivang(String loaivang) {
        this.loaivang = loaivang;
    }
    @Override
    public void nhap(){

        Scanner scanner = new Scanner(System.in);
        String mgd = scanner.nextLine();
        setMgd(mgd);
        System.out.println("Ma giao dịch : "+mgd);

        int ngaydg =scanner.nextInt();
        setNgaygd(ngaydg);
        int thangdg = scanner.nextInt();
        setThanggd(thangdg);
        int namdg = scanner.nextInt();
        setNamgd(namdg);
        System.out.println("Ngay giao dịch : "+ ngaydg+thangdg+namdg);
        int dongia = scanner.nextInt();
        setDongia(dongia);
        System.out.println("Đơn giá  : " + dongia);
        int soluong = scanner.nextInt();
        setDongia(soluong);
        System.out.println("Số lượng : " + soluong);
        String loaijvang = scanner.nextLine();
        setLoaivang(loaijvang);
        System.out.println("Loại vàng : " + loaijvang);
    }
    public void xuat(){
        System.out.println("Ma giao dich : "+getMgd()+" Ngay giao dịch : "+getNgaygd()+"/"+getThanggd()+"/"+getNamgd()+" Đơn giá  : "+getDongia()+" Số lượng : "+getSoluong()+" Loại vàng : "+getLoaivang());
        System.out.println();
    }

    public int sum(){
        System.out.println("Tổng số lượng : " + getSoluong());
        return getSoluong();
    }
    public float thanhtien(){
        float thanhtien = getSoluong()*getDongia();
        System.out.println("Thành tiền : "+thanhtien);
        return thanhtien;
    }
}
