package bai5;

import java.util.Scanner;

public class Tiente extends giaodich {
    private float tigia;
    private String loaiTien;

    public Tiente(String mgd, int ngaygd, int thanggd, int namgd, float dongia, int soluong, float tigia, String loaiTien) {
        super(mgd, ngaygd, thanggd, namgd, dongia, soluong);
        this.tigia = tigia;
        this.loaiTien = loaiTien;
    }
    public Tiente(){

    }

    public float getTigia() {
        return tigia;
    }

    public void setTigia(float tigia) {
        this.tigia = tigia;
    }

    public String getLoaiTien() {
        return loaiTien;
    }

    public void setLoaiTien(String loaiTien) {
        this.loaiTien = loaiTien;
    }
    @Override
    public void nhap(){
        Scanner scanner = new Scanner(System.in);
        String mgd = scanner.nextLine();
        setMgd(mgd);
        System.out.println("Ma giao dịch : "+mgd);

        int ngaydg =scanner.nextInt();
        setNgaygd(ngaydg);
        int thangdg = scanner.nextInt();
        setThanggd(thangdg);
        int namdg = scanner.nextInt();
        setNamgd(namdg);
        System.out.println("Ngay giao dịch : "+ ngaydg+thangdg+namdg);
        int dongia = scanner.nextInt();
        setDongia(dongia);
        System.out.println("Đơn giá  : " + dongia);
        int soluong = scanner.nextInt();
        setDongia(soluong);
        System.out.println("Số lượng : " + soluong);
        float tigia = scanner.nextFloat();
        setTigia(tigia);
        System.out.println("Tỉ giá : "+tigia);
        String loaitien = scanner.nextLine();
        setLoaiTien(loaitien);
        System.out.println("Loại tiền : " + loaitien);
    }

    public void xuat(){
        System.out.println("Ma giao dich : "+getMgd()+ " Ngay giao dịch : "+getNgaygd()+"/"+getThanggd()+"/"+getNamgd()+" Đơn giá  : "+getDongia()+" Số lượng : "+getSoluong()+ " Loại tiền : "+getLoaiTien()+" Tỉ giá : " + getTigia()+" Loại tiền : " + getLoaiTien());
        System.out.println();
    }
    public int sum(){
        System.out.println("Tổng số lượng : " + getSoluong());
        return getSoluong();
    }
    public float thanhtien(){
        float thanhtien;
        if(loaiTien == "USD" | loaiTien == "Euro"){
            thanhtien= getSoluong()*getDongia()*getTigia();
        }
        else {
            thanhtien= getSoluong()*getDongia();
        }
        return thanhtien;
    }
}
