package bai5;

public abstract class giaodich {
    private String mgd;
    private int ngaygd;
    private int thanggd;
    private int namgd;
    private float dongia;
    private int soluong;

    public giaodich(String mgd, int ngaygd, int thanggd, int namgd, float dongia, int soluong) {
        this.mgd = mgd;
        this.ngaygd = ngaygd;
        this.thanggd = thanggd;
        this.namgd = namgd;
        this.dongia = dongia;
        this.soluong = soluong;
    }

    public giaodich(){

    }

    public String getMgd() {
        return mgd;
    }

    public void setMgd(String mgd) {
        this.mgd = mgd;
    }

    public int getNgaygd() {
        return ngaygd;
    }

    public void setNgaygd(int ngaygd) {
        this.ngaygd = ngaygd;
    }

    public int getThanggd() {
        return thanggd;
    }

    public void setThanggd(int thanggd) {
        this.thanggd = thanggd;
    }

    public int getNamgd() {
        return namgd;
    }

    public void setNamgd(int namgd) {
        this.namgd = namgd;
    }

    public float getDongia() {
        return dongia;
    }

    public void setDongia(float dongia) {
        this.dongia = dongia;
    }

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }


    public abstract void nhap();
    public abstract void xuat();
    public abstract int sum();
    public abstract float thanhtien();
}
