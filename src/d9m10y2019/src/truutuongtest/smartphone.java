package truutuongtest;

public class smartphone extends p1 {
    private String type;
    private String camera;

    public smartphone(String type, String camera) {
        this.type = type;
        this.camera = camera;
    }
    public smartphone(){

    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    public String getCamera() {
        return camera;
    }

    public void setCamera(String camera) {
        this.camera = camera;
    }

    @Override
    public void showInfor(){
        System.out.println("type : " + getType());
        System.out.println("camera : "+getCamera());
  }

}
