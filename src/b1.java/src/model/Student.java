package model;

public class Student {
    private String StudentID;
    private int studentyear;
    private float studentAVGMark;
    private String faculty;



 // có 1 constructor ( phương thức) mặc định
//    public Student(){
//
//    }
//    public Student(String studentID){
//
//    }
//    public Student(String studentID , String faculty){
//
//    }
//    public Student (int studentyear){
//
//    }

// alt + insert + constructor : taoj ra cac gia tri ban dau


    // tên của thuộc tính  trùng với tên biến truyền vào thì nó ưu tiên biến truyền vào //
//    public Student(String studentID, int studentyear,
//                   float studentAVGMark, String faculty) {
//        StudentID = studentID;
//        this.studentyear = studentyear;
//        this.studentAVGMark = studentAVGMark;
//        this.faculty = faculty;
//    }

//    public void gotoSchool(){
//
//    }
//    public void study(){
//
//    }
//    public void doExample(){
//
//    }
//
//
//    public void getStudentResult(){
//
//    }
    public void setStudentID(String studentID) {
        this.StudentID = studentID;
    }

    public String getStudentID() {
        return StudentID;
    }



    public int getStudentyear() {
        return studentyear;
    }

    public void setStudentyear(int studentyear) {
        this.studentyear = studentyear;
    }

    public float getStudentAVGMark() {
        return studentAVGMark;
    }

    public void setStudentAVGMark(float studentAVGMark) {
        this.studentAVGMark = studentAVGMark;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }
}

