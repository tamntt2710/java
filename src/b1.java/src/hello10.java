import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class hello10 {
    public static void main(String[] args) {
        int[] arr = new int[]{5,2,4,1,3,2,3};// không dùng đc vì không phải lớp Integer
        String[] names = {"Than","Hoa","Dung","Tam"};
        Arrays.sort(names); // sort theo tên theo A-> Z
        String res = Arrays.toString(names); // in ra dưới dạng mảng
        System.out.println(res);
      //  Arrays.sort(names, Collections.reverseOrder()); // sắp xếp theo thứu tự giảm dần
       // String tmp = Arrays.toString(names);
        //System.out.println(tmp);
       // int index = Arrays.binarySearch(names,"Hoa");// tìm 1 tên trong mảng, nếu ko tồn tại trả về giá trị bất kì <0

        //int index = Arrays.binarySearch(names,0,5,"Hoa" ); // tìm từ vị trí 1 đến 5
        //System.out.println("index = " + index);


        String[] name2 = Arrays.copyOf(names,1); // copy 3 phần tử
        System.out.println(Arrays.toString(name2));
        System.out.println("is equal ? " + Arrays.equals(names,name2));



        int[] myarr = new int[20];
        Arrays.fill(myarr,99); // gắn cho tất cả các phần tử trong mảng 1 giá trị
    }

}
