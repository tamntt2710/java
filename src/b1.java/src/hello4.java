import java.util.Scanner;

public class hello4 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
//        System.out.println("Nhap vao 1 so nguyen : ");
//        int a = scanner.nextInt();
//        System.out.println("Nhap vao 1 so thuc: ");
//        float floatValue = scanner.nextFloat();
//        System.out.println("Nhap vao 1 double: ");
//        double doubleValue = scanner.nextDouble();
//        System.out.println(a+"-"+floatValue+"-"+doubleValue);


        System.out.println("Nhap vao 1 ki tu : ");
        char c = scanner.next().charAt(0);//Đọc vào 1 kí tự
        System.out.println("Ban vua nhap "+ c);

        System.out.println("Nhap vao 1 tu : ");
        String word = scanner.next(); // Đọc vào 1 từ
        System.out.println("Tu ban vua nhap: "+word);

        scanner.nextLine();//du lieu khong bi tran giong fflush(stdin);
        System.out.println("Nhap vao 1 dong : ");
        String line = scanner.nextLine();//ĐỌc vào cả dòng;
        System.out.println("Do1ng ban vua nhap " + line);


        System.out.println("Nhap vao T/F");
        boolean booleanValue = scanner.hasNextBoolean(); // đọc vào 1 giá trị T/F
        System.out.println("Gia tri vua nhap : "+ booleanValue);
    }
}
