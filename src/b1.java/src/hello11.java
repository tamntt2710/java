import java.util.Scanner;

public class hello11 {
    public static void main(String[] args) {
        /*
        mảng hai chiều
         */
        int[][] ar = new int[4][3];

        boolean[][] booleans = new boolean[9][9];

        String[][] strings = new String[2][3];

        int[][] ar2 = {
                {1, 2, 3},
                {3, 2, 3},
                {2, 3, 4}
        };//mảng khởi tạo mặc định
//        Scanner scanner = new Scanner(System.in);
//        int i,j;
//        for(i=0;i<ar.length;i++)
//            for(j=0;j<ar[0].length;j++)
//            {
//                System.out.println("ar[i][j] = ");
//                ar[i][j]=scanner.nextInt();
//            }
//        for(i=0;i<ar.length;i++) {
//            for (j = 0; j < ar[0].length; j++) {
//                System.out.print(ar[i][j] + " ");
//            }
//            System.out.println();
//        }
        // each for mảng 1 chiều
//        int[] x = new int[10];
//        for(int a:x){
//            System.out.println(a);
//        }
        //  Truy xuất với for each , không dùng để gán
        for (int[] x : ar2) {//truy xuất theo hàng
            for (int y : x) {// truy xuất theo cột
                System.out.print(y + " ");
            }
            System.out.println();
        }
    }
}




        // truy xuất với String
//        for(String[] x: strings) {//truy xuất theo string
//            for (String y : x) {
//                System.out.print(y + " ");
//            }
//            System.out.println();
//        }

        // mảng dích dắc;
//        int[][] arr3 = new int[10][];//cấp phát số hàng trước
//        for(int i=0 ;i<arr3.length;i++){
//            arr3[i]=new int[i+1];
//        }
//        for(int i=0;i<arr3.length;i++)
        //    System.out.println("Socot cua hang thu " + i + ": " + arr3 );
//}
