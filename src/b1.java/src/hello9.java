import java.util.Scanner;

public class hello9 {
    /*
    array : là cấu trúc cá phân tưt có cùng 1 kiểu dữ liệu
    các phần tử trong 1 mảng có cùng 1 kiểu dữ liểu , nằm trong 1 ô nhớ gần nhau
     */
    public static void main(String[] args) {
        int [] arr = new int[100]; // đặt tên cho mảng là arr, có khả năng lưu trữ 100 phần tử
        int i;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Nhap gia tri cua n : ");
        int n = scanner.nextInt();
        for(i=0;i<n;i++)
        {
            System.out.println("a[i] = ");
             arr[i]= scanner.nextInt();
        }
        for(i=0;i<n;i++)
        {
            System.out.printf(arr[i] + " ");
        }

    }
    // nếu là kiểu dữ liệu string thì các phần tử tự gán là NULL , boolen : false
}
