package act3;

import java.util.Scanner;
import java.util.Date;
import java.text.SimpleDateFormat;

public class HangThucPham {
    private String mahang;
    private String tenhang;
    private int dongia;
    private String ngaysx;
    private String ngayhethan;

    public HangThucPham(String mahang, String tenhang, int dongia, String ngaysx, String ngayhethan) {
        this.mahang = mahang;
        this.tenhang = tenhang;
        this.dongia = dongia;
        this.ngaysx = ngaysx;
        this.ngayhethan = ngayhethan;
    }

    public HangThucPham(String mahang) {
        this.mahang = mahang;
    }

    public HangThucPham() {

    }

    public String getMahang() {
        return mahang;
    }

    public void setMahang(String mahang) {
        this.mahang = mahang;
    }

    public String getTenhang() {
        return tenhang;
    }

    public void setTenhang(String tenhang) {
        this.tenhang = tenhang;
    }

    public int getDongia() {
        return dongia;
    }

    public void setDongia(int dongia) {
        this.dongia = dongia;
    }

    public String getNgaysx() {
        return ngaysx;
    }

    public void setNgaysx(String ngaysx) {
        this.ngaysx = ngaysx;
    }

    public String getNgayhethan() {
        return ngayhethan;
    }

    public void setNgayhethan(String ngayhethan) {
        this.ngayhethan = ngayhethan;
    }



    public void NhapTT() {
        System.out.println("Nhap ma hang : ");
        Scanner scanner = new Scanner(System.in);
        mahang = scanner.nextLine();
        System.out.println("Nhap ten hang : ");
        mahang = scanner.nextLine();
        System.out.println("Nhap don gia : ");
        dongia = scanner.nextInt();
        System.out.println("Nhap ngay san xuat : ");
        ngaysx = scanner.nextLine();
        System.out.println("Nhap het han : ");
        ngayhethan = scanner.nextLine();
    }

    public void result() {
        System.out.println("Ma hang : " + mahang);
        System.out.println("Ten hang : " + tenhang);
        System.out.println("Don gia : " + dongia);
        System.out.println("Ngay san xuat : " + ngaysx);
        System.out.println("Ngay het han : " + ngayhethan);

    }
}
