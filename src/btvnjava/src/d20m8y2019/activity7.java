package d20m8y2019;

import java.util.Scanner;

//7. Sử dụng mảng 1 chiều nhập vào dãy số nguyên gồm n phần tử. In ra phần tử MAX, MIN và số thứ tự của chúng trong dãy.
public class activity7 {
    public static void input(int[] a, int n) {
        System.out.println("Nhap mang :");
        for (int i = 0; i < n; i++) {
            System.out.println("a[i] =");
            Scanner scanner = new Scanner(System.in);
            a[i] = scanner.nextInt();
        }
    }

    public static void output(int[] a, int n) {
        System.out.println("Xuat mang : ");
        for (int i = 0; i < n; i++) {
            System.out.println(a[i]);
        }
    }

    public static void timMax(int[] a, int n) {
        int max = a[0];
        int vt = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] > max) {
                max = a[i];
                vt = i;
            }
        }
        System.out.println("Giá trị lớn nhất là " + max + " vị trí " + vt);

    }

    public static void timMin(int[] a, int n) {
        int min = a[0];
        int vt = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] < min) {
                min = a[i];
                vt = i;
            }
        }
        System.out.println("Giá trị nhỏ nhất là " + min + " vị trí " + vt);

    }

    public static void main(String[] args) {
        int n;
        System.out.println("Nhap số phần tử của mảng : ");
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        int[] a = new int[n];
        input(a, n);
        timMax(a, n);
        timMin(a, n);
    }
}
