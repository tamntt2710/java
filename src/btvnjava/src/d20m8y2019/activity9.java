package d20m8y2019;

import java.util.Scanner;

//9. Nhập mảng 2 chiều các số nguyên có dạng ma trận 3x4. Liệt kê các phần tử là số nguyên tố trong có trong mảng.
public class activity9 {
    public static void input(int[][] a, int n, int m) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                Scanner scanner = new Scanner(System.in);
                System.out.println("a" + "[" + i + "]" + "[" + j + "]" + " = " );
                a[i][j] = scanner.nextInt();
            }
        }
    }
    public static void SNT(int n) {
        int ktra = 1;
        if (n == 2) {
            ktra = 1;
        }
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                ktra = 0;
            }
        }
        if (ktra == 1) {
            System.out.println(n);
        }
    }

    public static void main(String[] args) {
        int[][] a = new int[3][4];
        input(a,3,4);
        for(int i = 0 ;i <3 ;i++){
            for(int j = 0;j <4;j++){
                SNT(a[i][j]);
            }
        }
    }
}
