package d20m8y2019;

import java.util.Scanner;

//4. Sử dụng mảng 1 chiều nhập vào dãy số nguyên gồm n phần tử. Nhập 1 số nguyên k bất kì. Kiểm tra k có thuộc mảng hay không và xuất hiện mấy lần trong mảng.
public class activity4 {
    public static void input(int[] a, int n) {
        System.out.println("Nhap mang :");
        for (int i = 0; i < n; i++) {
            System.out.println("a[i] =");
            Scanner scanner = new Scanner(System.in);
            a[i] = scanner.nextInt();
        }
    }

    public static void main(String[] args) {
        System.out.println("Nhập số phần tử của mảng : ");
        Scanner scanner = new Scanner(System.in);
        int n= scanner.nextInt();
        int[] a = new int[n];
        input(a,n);
        System.out.println("Nhap giá trị bất kì : ");
        int m = scanner.nextInt();
        int dem = 0;
        for(int i = 0;i<n ;i++){
            if(m == a[i]){
                dem++;
            }
        }
        if(dem == 0){
            System.out.println("Số đó không có trong mảng ");
        }
        else
        {
            System.out.println("Số " + m + " xuất hiện " + dem + "lần");
        }

    }
}
