package d20m8y2019;

//1. Sử dụng mảng 1 chiều nhập vào dãy số nguyên gồm n phần tử. Sắp xếp các phần tử theo thứ tự tăng dần, giảm dần ( Sử dụng switch...case).

import java.util.Scanner;

public class activity1 {
    public static void input(int[] a, int n) {
        System.out.println("Nhap mang :");
        for (int i = 0; i < n; i++) {
            System.out.println("a[i] =");
            Scanner scanner = new Scanner(System.in);
            a[i] = scanner.nextInt();
        }
    }

    public static void output(int[] a, int n) {
        System.out.println("Xuat mang : ");
        for (int i = 0; i < n; i++) {
            System.out.println(a[i]);
        }
    }

    public static void sapxep(int[] a, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (a[i] < a[j]) {
                    int tmp;
                    tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
            }
        }

    }
    public static void tang(int[] a, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (a[i] > a[j]) {
                    int tmp;
                    tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
            }
        }

    }

    public static void main(String[] args) {
        int n;
        System.out.println("Nhap số phần tử của mảng : ");
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        int[] a = new int[n];
        input(a, n);
        System.out.println("Nhâp 1 : sắp xếp giam và 0 nếu sắp xếp tang ");
        int m = scanner.nextInt();
        switch (m) {
            case 1:
                sapxep(a, n);
                output(a, n);
                break;
            case 0:
                tang(a, n);
                output(a, n);
                break;
        }
    }

}
