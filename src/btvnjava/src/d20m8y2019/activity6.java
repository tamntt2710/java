package d20m8y2019;

import java.util.Scanner;

//6. Sử dụng mảng 1 chiều nhập vào dãy số nguyên dương gồm n phần tử. Sắp xếp lại dãy số theo dạng, nửa đầu của dãy là các số nguyên dương theo thứ tự tăng dần, nửa sau của dãy là các số nguyên âm sắp xếp theo thứ tự giảm dần.
//        INPUT: -3, 4, 6, -5, 3, 8, -2, -1
//        OUTPUT: 3, 4, 6, 8, -1, -2, -3, -5
public class activity6 {
    public static void input(int[] a, int n) {
        System.out.println("Nhap mang :");
        for (int i = 0; i < n; i++) {
            System.out.println("a[i] =");
            Scanner scanner = new Scanner(System.in);
            a[i] = scanner.nextInt();
        }
    }

    public static void output(int[] a, int n) {
        for (int i = 0; i < n; i++) {
            System.out.println(a[i]);
        }
    }

    public static void sapxep(int[] a, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (a[i] < a[j]) {
                    int tmp;
                    tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
            }
        }

    }

    public static void tang(int[] a, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                if (a[i] > a[j]) {
                    int tmp;
                    tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
            }
        }

    }

    public static void main(String[] args) {
        int n;
        System.out.println("Nhap số phần tử của mảng : ");
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        int[] a = new int[n];
        input(a, n);
        int[] b = new int[100];
        int[] c = new int[100];
        int  k = 0 , j= 0 ;
        for(int i = 0 ;i <n ; i++){
            if (a[i] > 0) {
                b[j] = a[i];
                j++;
            }
            else {
                c[k] = a[i];
                k++;
            }
            }
        System.out.println("k = " + k + " j = "+ j);
        System.out.println("Xuat mang : ");
        tang(b, j);
        output(b, j);
        sapxep(c, k);
        output(c, k);
        }
    }
