package d20m8y2019;

import java.util.Scanner;

//2. Sử dụng mảng 1 chiều nhập vào dãy số nguyên gồm n phần tử. Tính tổng các phần tử thuộc mảng.
public class activity2 {
    public static void input(int[] a, int n) {
        System.out.println("Nhap mang :");
        for (int i = 0; i < n; i++) {
            System.out.println("a[i] =");
            Scanner scanner = new Scanner(System.in);
            a[i] = scanner.nextInt();
        }
    }

    public static void main(String[] args) {
        int n;
        System.out.println("Nhap số phần tử của mảng : ");
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        int[] a = new int[n];
        input(a, n);
        int s = 0;
        for (int i = 0; i < n; i++) {
            s += a[i];
        }
        System.out.println("Tổng là : " + s);
    }
}
