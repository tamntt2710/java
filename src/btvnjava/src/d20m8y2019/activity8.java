package d20m8y2019;

import java.util.Scanner;

//8. Sử dụng mảng 1 chiều in ra dãy số Fibonasi với n phần tử (n>0 )
public class activity8 {
    public static int Fibonasi(int n) {
        int a1 = 1, a2 = 1;
        if (n == 1 || n == 2)
            return 1;
        int i = 3;
        int a = 0;
        while (i <= n) {
            a = a1 + a2;
            a1 = a2;
            a2 = a;
            i++;
        }
        return a;
    }
    public static void output(int[] a, int n) {
        System.out.println("Xuat mang : ");
        for (int i = 0; i < n; i++) {
            System.out.println(a[i]);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap giá trị của n : ");
        int n = scanner.nextInt();
        int[] a = new int[n];
        int i = 0;
        int j = 1;
        while( i < n ){
                a[i] = Fibonasi(j);
                i++;
                j++;
            }
        output(a,n);
        }

}
