package d20m8y2019;

import java.util.Scanner;

//5. Sử dụng mảng 1 chiều nhập vào dãy số nguyên gồm n phần tử. Nhập 1 số nguyên dương k bất kì (k<n). In ra phần tử tại vị trí k trong mảng.
public class activity5 {
    public static void input(int[] a, int n) {
        System.out.println("Nhap mang :");
        for (int i = 0; i < n; i++) {
            System.out.println("a[i] =");
            Scanner scanner = new Scanner(System.in);
            a[i] = scanner.nextInt();
        }
    }

    public static void main(String[] args) {
        System.out.println("Nhập số phần tử của mảng : ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        input(a, n);
        System.out.println("Nhap giá trị bất kì : ");
        int k = scanner.nextInt();
        System.out.println(a[k]);
    }
}
