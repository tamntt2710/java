package d20m8y2019;

import java.util.Scanner;

//12. Nhập và mảng A, B là mảng 2 chiều. Các phân tử trong mảng là các số nguyên. B có phải là ma trận nghịch đảo của A hay không?
public class activity12 {
        public static void input(int[][] a, int n, int m, char name) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    Scanner scanner = new Scanner(System.in);
                    System.out.println(name + "[" + i + "]" + "[" + j + "]" + " = ");
                    a[i][j] = scanner.nextInt();
                }
            }
        }

        public static void output(int[][] a, int n, int m) {
            System.out.println("Xuat mang : ");
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    System.out.println(a[i][j]);
                }

            }
        }

        public static void nhanmang(int[][] a, int[][] b, int[][] c, int n, int m, int p) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < p; j++) {
                    a[i][j] = 0;
                    for (int k = 0; k < m; k++) {
                        a[i][j] += b[i][k] * c[k][j];
                    }
                }
            }

        }

        public static void ktra(int[][] a, int n, int m) {
            int ktra = 1;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if (i == j ) {
                        if(a[i][j] == 1){
                            if(i != j){
                                if(a[i][j] == 0){
                                    ktra = 1;
                                }
                                else
                                    ktra = 0;
                            }
                        }
                    }
                }
            }
            if (ktra == 1) {
                System.out.println("B la ma tran nghich dao cua A ");
            } else {
                System.out.println("B khong la ma tran nghich dao cua A");
            }

        }

        public static void main(String[] args) {
            System.out.println("Nhap n,m,p : ");
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();
            int m = scanner.nextInt();
            int p = scanner.nextInt();
            int[][] a = new int[n][p];
            int[][] b = new int[n][m];
            int[][] c = new int[m][p];
            input(b, n, m, 'b');
            input(c, m, p, 'c');
            nhanmang(a, b, c, n, m, p);
            output(a, n, p);
            ktra(a, n, p);
        }
    }
