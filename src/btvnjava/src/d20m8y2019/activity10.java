package d20m8y2019;

import java.util.Scanner;

//10. Nhập mảng 2 chiều các số nguyên dương có dạng là ma trận 3x3. Sắp xếp lại các phần tử trong mảng theo thứ tự tăng dần.
public class activity10 {
    public static void input(int[][] a, int n, int m) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                Scanner scanner = new Scanner(System.in);
                System.out.println("a" + "[" + i + "]" + "[" + j + "]" + " = ");
                a[i][j] = scanner.nextInt();
            }
        }
    }

    public static void output(int[][] a, int n, int m) {
        System.out.println("Xuat mang : ");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.println(a[i][j]);
            }

        }
    }

    public static void sapxep(int[][] a, int n, int m) {
        for (int i = 0; i<n; i++) {
            for (int j = 0; j < m; j++) {
                if (a[i][j] < a[i + 1][j]) {
                    int tmp = a[i][j];
                    a[i][j] = a[i + 1][j];
                    a[i + 1][j] = tmp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[][] a = new int[3][3];
        input(a, 3, 3);
        sapxep(a, 3, 3);
        output(a, 3, 3);
    }

}
