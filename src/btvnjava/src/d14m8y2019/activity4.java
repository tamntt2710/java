package d14m8y2019;

import java.util.Scanner;

//Bài 4: Hãy đếm số lượng chữ số nhỏ nhất của số nguyên dương n.
public class activity4 {
    public static int findMin(int n){
        int m ;
        int min =9;
        while(n>0)
        {
            m = n % 10;
            if(m <  min)
            {
                min = m;
            }
            n/=10;
        }
        System.out.println("min = " + min);
        return min;
    }
    public static int dem(int n){
        int x = findMin(n);
        int dem=0;
        int m;
        while(n>0)
        {
            m = n % 10;
            if(m == x )
            {
               dem++;
            }
            n/=10;
        }
        return dem;
    }
    public static void main(String[] args) {
        System.out.println("Nhap n : ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int dem = dem(n);
        System.out.println("so lan xuat hien la : " + dem);
    }
}
