package d14m8y2019;

import java.util.Scanner;

//Bài 15: In ra màn hình tam giác cân có cạnh n
public class activity15 {
    public static void main(String[] args) {
        System.out.println("Nhap canh tam giac ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int i,j;
        for(i=1;i<=n;i++) {
            for (j = 1; j <= n - i; j++)
                System.out.println(" ");
            for (j = 1; j <= 2 * i - 1; j++)
                System.out.println("*");
            System.out.println("\n");
        }
    }
    }

