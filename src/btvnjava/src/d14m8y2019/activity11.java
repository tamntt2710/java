package d14m8y2019;

import java.util.Scanner;

//Bài 11: Viết chương trình In ra tất cả các số lẻ nhỏ hơn 100 trừ các số 5, 7, 93.
public class activity11 {
    public static void main(String[] args) {
        for(int i = 0 ;i<100 ;i++)
        {
            if(i %2 != 0 )
            {
                if(i != 5 && i!=7 && i!= 93)
                    System.out.println(i);
            }
        }
    }
}
