package d14m8y2019;

import java.util.Scanner;

//Bài 3: Tìm chữ số lớn nhất của số nguyên dương n.
public class activity3 {
    public static void main(String[] args) {
        System.out.println("Nhap n : ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int max = 0;
        int m;
        while (n > 0) {
            m = n % 10;
            if (m > max) {
                max = m;
            }
            n = n / 10;
        }
        System.out.println("Chu so lon nhat la : " + max);
    }


}
