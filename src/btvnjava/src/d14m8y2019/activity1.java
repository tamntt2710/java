package d14m8y2019;

import java.util.Scanner;

//Bài 1: Tính tổng các chữ số chẵn của số nguyên dương n
public class activity1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println("n = " + n);
        int m;
        int s = 0;
        while (n > 0) {
            m = n % 10;
            if (m % 2 == 0) {
                s += m;
            }
            n = n / 10;
        }
        System.out.println("tong cac chu so chan la : " + s);
    }
}
