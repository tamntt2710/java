package d14m8y2019;

import java.util.Scanner;

//Bài 10: Viết chương trình kiểm tra một số có phải số nguyên tố hay không.
public class activity10 {
    public static void SNT(int n){
        int ktra =1;
        for(int i = 2;i <n ;i++)
        {
            if(n %i == 0)
            {
                ktra = 0;
                System.out.println("Khong la so nguyen to");
                break;
            }
            else
            {
                ktra =1;
            }
        }
        if(ktra == 1)
        {
            System.out.println("La so nguyen to ");
        }
    }
    public static void main(String[] args) {
        System.out.println("Nhap n : ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        SNT(n);
    }
}
