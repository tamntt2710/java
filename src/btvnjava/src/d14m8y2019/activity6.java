import java.util.Scanner;

public class activity6 {
    //Bài 6: Hãy kiểm tra các chữ số của số nguyên dương n có giảm dần từ trái sang phải hay không?
    public static void main(String[] args) {
        System.out.println("Nhap n : ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int ktra = 1;
        int m;
        int test = 0;
        while (n > 0) {
            m = n % 10;
            if (m > test ) {
                test = m;
                ktra = 1;
                n/=10;
            }
            else
            {
                ktra = 0;
                System.out.println(" khong giam dan tu trai qua phai");
                break;
            }
        }
        if(ktra == 1){
            System.out.println("La so giam dan tu trai qua phai");
        }
    }
}
