package d14m8y2019;

import java.util.Scanner;

//Bài 2: Đếm số lượng số lẻ của số nguyên dương n
public class activity2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println("n = " + n);
        int m;
        int dem = 0;
        while (n > 0) {
            m = n % 10;
            if (m % 2 != 0) {
                dem ++;
            }
            n = n / 10;
        }
        System.out.println("cac chu so  la : " + dem);
    }
}
