package d14m8y2019;
//Bài 5: Hãy kiểm tra số nguyên dương n có toàn chữ số lẻ/chẵn hay không?

import java.util.Scanner;

public class activity5 {
    public static void testChan(int n){
        int m;
        int ktra = 1;
        while (n > 0) {
            m = n % 10;
            if (m % 2 != 0) {
                ktra = 0;
                System.out.println("Khong la so hoan toan chan");
                break;
            }
            else{
                ktra =1;
                n = n / 10;
            }
        }
        if(ktra == 1){
            System.out.println("la so chan hoan toan ");
        }

    }

    public static void test(int n){
        int m,ktra = 1;
        while (n > 0) {
            m = n % 10;
            if (m % 2 == 0) {
                ktra = 0;
                System.out.println("Khong la so hoan toan le");
                break;
            }
            else{
                n = n / 10;
                ktra = 1;
            }
        }
        if(ktra == 1)
        {
            System.out.println("So toan le");
        }


    }

    public static void main(String[] args) {
        System.out.println("Nhap n : ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        //testChan(n);
        test(n);
    }
}
