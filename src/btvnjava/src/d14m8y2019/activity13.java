package d14m8y2019;

import java.util.Scanner;

//Bài 13: In ra màn hình hình vuông rỗng có cạnh n
public class activity13 {
    public static void main(String[] args) {
        System.out.println("Nhap canh hinh vuong ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 1; i <= n; i++) {
            for (int k = 1; k <= n; k++) {
                if (i == 1 || k == 1 || i == n || k == n) {
                    System.out.println("*");
                } else {
                    System.out.println(" ");
                }
            }
            System.out.println("\n");
        }
    }
}
