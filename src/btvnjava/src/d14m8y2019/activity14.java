package d14m8y2019;

import java.util.Scanner;

//Bài 14: In ra màn hình tam giác vuông cân có cạnh n
public class activity14 {
    public static void main(String[] args) {
        System.out.println("Nhap canh tam giac ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for(int i = 1; i <= n; i++)
        {
            for(int k = 1; k <= i; k++)
            {
                System.out.println("*");
            }
            System.out.println("\n");
        }
    }
}
